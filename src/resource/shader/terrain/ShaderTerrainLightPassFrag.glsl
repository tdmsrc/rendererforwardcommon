#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texHeightAndAux;
uniform sampler2D texNormalAndBlend;

uniform sampler2D texColor1;
uniform sampler2D texColor2;

varying vec3 vertexPosAmbient;
varying vec2 texDataCoord;
varying vec2 texColorCoord;

varying float intensityFog; 


#shared getPhong
#shared shadowFilter
	

void main()
{
	//read from data
	vec4 dataHeightAndAux = texture2D(texHeightAndAux, texDataCoord);
	vec4 dataNormalAndBlend = texture2D(texNormalAndBlend, texDataCoord);
	
	
	//get frag color
	float fragHeight = dataHeightAndAux.r*255.0 + dataHeightAndAux.g;
	float weight = fragHeight/256.0;
	
	vec3 colorL = vec3(0.0, 1.0, 0.0);
	vec3 colorH = vec3(1.0, 0.0, 0.0);
	vec3 hColor = weight * colorH + (1.0 - weight) * colorL;
	
	vec3 color1 = vec3(texture2D(texColor1, texColorCoord));
	vec3 color2 = vec3(texture2D(texColor2, texColorCoord));
	vec3 tColor = weight * color1 + (1.0 - weight) * color2;
	
	vec3 mtlColor = 0.9*tColor + 0.1*hColor;
	
	
	//get position and normal
	float nx = 2.0*dataNormalAndBlend.r - 1.0;
	float nz = 2.0*dataNormalAndBlend.g - 1.0;
	vec3 bumpedNormal = vec3(nx, sqrt(1-nx*nx-nz*nz), -nz);
	vec3 fragPosition = vertexPosAmbient;


	//perform shadow mapping (adjust position to avoid z-fighting)
	vec3 fragPositionSM = fragPosition + 0.05*bumpedNormal;
	
	float fragLightPercentage;
	bool fragShadowed;
	shadowFilter(fragPositionSM, bumpedNormal, fragLightPercentage, fragShadowed);
	
	if(fragShadowed){ discard; }
	else{
		//return phong color
		vec3 phongColor = getPhong(mtlColor, bumpedNormal, fragPosition);
		phongColor *= fragLightPercentage * (1.0 - intensityFog);
		gl_FragColor = vec4(phongColor, 1.0);
	}
}