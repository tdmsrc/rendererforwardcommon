
//tile position
attribute vec2 vertexPosition;

//height data
uniform sampler2D texHeightAndAux;

//terrain metrics
uniform vec2 terrainPosMin, terrainPosMax;
uniform vec2 terrainDataTexMin, terrainDataTexMax;
uniform vec2 terrainColorTexMin, terrainColorTexMax;
uniform float terrainHeightScale;

//tile metrics
uniform vec2 tileLerpMin, tileLerpMax;

varying vec2 texDataCoord;
varying vec2 texColorCoord;


//set texDataCoord and texColorCoord, and return pre-objTrans ambient position
//must match what is in TerrainMetrics.getDataTexturePosition
vec3 extractData(){

	vec2 vertexLerp = tileLerpMin + vertexPosition*(tileLerpMax - tileLerpMin);
	
	//lerp from tile metric data
	vec2 vertexPosFlat = terrainPosMin + vertexLerp*(terrainPosMax - terrainPosMin);
	texDataCoord = terrainDataTexMin + vertexLerp*(terrainDataTexMax - terrainDataTexMin);
	texColorCoord = terrainColorTexMin + vertexLerp*(terrainColorTexMax - terrainColorTexMin);
	
	vec4 dataHeightAndAux = texture2D(texHeightAndAux, texDataCoord);
	float vertexHeight = dataHeightAndAux.r*255.0 + dataHeightAndAux.g;
	return vec3(vertexPosFlat.x, vertexHeight*terrainHeightScale, -vertexPosFlat.y);
}