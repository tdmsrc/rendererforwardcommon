//transformations
uniform vec3 eyePosition;
uniform mat3 modelViewRotation;
uniform mat4 projectionMatrix;


#shared extractData


void main(){

	vec3 vertexPosPreTransform = extractData();
	
	vec3 vertexPosAmbient = vertexPosPreTransform;
	vec3 vertexPosCam = modelViewRotation * (vertexPosAmbient - eyePosition);
	
	gl_Position = projectionMatrix * vec4(vertexPosCam, 1.0);
}