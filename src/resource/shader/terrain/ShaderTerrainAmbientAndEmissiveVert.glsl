//transformations
uniform vec3 eyePosition;
uniform mat3 modelViewRotation;
uniform mat4 projectionMatrix;

varying vec3 vertexPosAmbient;
varying float intensityFog; 


#shared getFogIntensity
#shared extractData


void main(){

	vec3 vertexPosPreTransform = extractData();
	
	vertexPosAmbient = vertexPosPreTransform;
	vec3 vertexPosCam = modelViewRotation * (vertexPosAmbient - eyePosition);
	intensityFog = getFogIntensity(-vertexPosCam.z);
	
	gl_Position = projectionMatrix * vec4(vertexPosCam, 1.0);
}