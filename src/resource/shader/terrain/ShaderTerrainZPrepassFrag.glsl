#ifdef GL_ES
precision highp float;
#endif


uniform float opacity;
uniform vec3 fogColor;

varying float intensityFog;


void main()
{
	gl_FragColor = vec4(fogColor*intensityFog, opacity);
}