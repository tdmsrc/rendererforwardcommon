
attribute vec3 vertexPosition;
attribute vec2 vertexTex;

uniform mat3 modelViewRotation;
uniform mat4 projectionMatrix;

varying vec2 texCoord;


void main(){

	texCoord = vertexTex;

	vec3 vertexPosAmbient = vertexPosition;
	vec3 vertexPosCam = modelViewRotation * vertexPosAmbient;

	//xyww swizzle to put vertex at far plane
	gl_Position = (projectionMatrix * vec4(vertexPosCam, 0.0)).xyww;	
}