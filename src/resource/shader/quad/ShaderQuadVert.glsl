
attribute vec3 vertexPosition;
attribute vec2 vertexTex;

varying vec2 texCoord;

void main(){

	texCoord = vertexTex;
	gl_Position = vec4(vertexPosition, 1.0);
}