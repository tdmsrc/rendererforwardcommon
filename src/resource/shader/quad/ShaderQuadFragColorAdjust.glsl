#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;

uniform float colorAdjustBrightness;
uniform float colorAdjustContrast;
uniform float colorAdjustSaturation;

varying vec2 texCoord;


void main()
{
	vec3 color = texture2D(texColor, texCoord).rgb;
	
	//adjust saturation
	float lum = dot(color, vec3(0.299,0.587,0.144));
	vec3 noSaturation = vec3(lum, lum, lum);
	color = mix(noSaturation, color, colorAdjustSaturation);
	
	//adjust brightness
	color += vec3(colorAdjustBrightness, colorAdjustBrightness, colorAdjustBrightness);
	
	//adjust contrast
	vec3 noContrast = vec3(0.5, 0.5, 0.5);
	color = mix(noContrast, color, colorAdjustContrast);
	
	gl_FragColor = vec4(color, 1.0); 
}