#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;
uniform sampler2D texBlur;

uniform sampler2D texDepth;

uniform float zNearInv;
uniform float zFarInv;
uniform float dFocusTarget;
uniform float dFocusRangeInv;

varying vec2 texCoord;


void main()
{
	//get distance from camera (formula obtained by applying gluPerspective to (0,0,d,1) and inverting)
	float d = 1.0 / mix(zNearInv, zFarInv, texture2D(texDepth, texCoord).z);
	
	//get blend value (=0 at dFocusTarget, =1 for |d-dFocusTarget| >= dFocusRange)
	float dScale = (d-dFocusTarget) * dFocusRangeInv;
	float a = clamp(dScale*dScale, 0.0, 1.0);
	
	//blend blur and non-blur textures, but preserve the original non-blurred alpha value
	vec4 colorNonBlur = texture2D(texColor, texCoord);
	vec4 colorBlur = texture2D(texBlur, texCoord);
	gl_FragColor = mix(colorNonBlur, colorBlur, a);
}