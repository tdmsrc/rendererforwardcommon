#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;

uniform float alpha;

varying vec2 texCoord;


void main()
{
	gl_FragColor = vec4(texture2D(texColor, texCoord).rgb, alpha);
}