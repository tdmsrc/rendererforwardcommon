#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor; //actually a depth texture
uniform sampler2D texDepth;

uniform float zNearInv;
uniform float zFarInv;

varying vec2 texCoord;


//get distance from camera (formula obtained by applying gluPerspective to (0,0,d,1) and inverting)
float getDepth(vec2 texPosition){

	return 1.0 / mix(zNearInv, zFarInv, texture2D(texColor, texPosition).z);
}

void main()
{
	//sample a bunch of depths
	float ds = 1.0 / 512.0;
	float dt = 1.0 / 512.0;
	
	float dFrag = getDepth(texCoord);
	
	float diffSum = 0.0;
	
	for(int is = -4; is<=4; is++){ 
	for(int it = -4; it<=4; it++){ 
		vec2 texCoordOffset = vec2(ds*float(is), dt*float(it));
		float d = getDepth(texCoord + texCoordOffset);
		
		diffSum += clamp(dFrag - d, 0.0, 1.0); //upper value changes sensitivity to depth diffs
	}}
	
	float diffAvg = diffSum / 81.0;
	float c = 1.0 * diffAvg; //scale by 1 over the upper value in above clamp
	
	//adjust
	c = clamp(0.5*c, 0.0, 1.0); 
	
	//gl_FragColor = vec4(vec3(1.0,1.0,1.0) * (1.0 - c), 2.0 * (c - 0.5) * (c - 0.5)); 
	gl_FragColor = vec4(0.0, 0.0, 0.0, c);
}