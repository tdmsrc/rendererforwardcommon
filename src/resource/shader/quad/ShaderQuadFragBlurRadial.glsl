#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;

uniform vec2 blurCenter;

varying vec2 texCoord;


void main()
{
	vec2 lightSource = blurCenter;
	
	//sample n points evenly spaced along a segment starting at texCoord and ending at lightSource
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
	
	//int n = 100;
	for(int i=0; i<100; i++){
		float t = float(i) * 0.01; // *1/n
		color += texture2D(texColor, mix(texCoord, lightSource, t)) * 0.01; // *1/n
	}
	
	gl_FragColor = vec4(color.rgb, 1.0);
}