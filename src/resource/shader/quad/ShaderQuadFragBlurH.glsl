#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;
uniform ivec2 texSize;

varying vec2 texCoord;


void main()
{
	float pw = 1.0/float(texSize.x);
	float ph = 1.0/float(texSize.y);
	
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
	
	color += 0.0702702703 * texture2D(texColor, texCoord + vec2(-3.2307692308 * pw, 0.0));
	color += 0.3162162162 * texture2D(texColor, texCoord + vec2(-1.3846153846 * pw, 0.0));
	color += 0.2270270270 * texture2D(texColor, texCoord + vec2( 0.0          * pw, 0.0));
	color += 0.3162162162 * texture2D(texColor, texCoord + vec2( 1.3846153846 * pw, 0.0));
	color += 0.0702702703 * texture2D(texColor, texCoord + vec2( 3.2307692308 * pw, 0.0));
	
	gl_FragColor = color;
}