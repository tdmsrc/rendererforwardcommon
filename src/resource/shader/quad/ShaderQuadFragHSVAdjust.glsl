#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;

uniform float hsvAdjustHue;
uniform float hsvAdjustSaturation;
uniform float hsvAdjustValue;

varying vec2 texCoord;

//use: HSV color has h in degrees (not neccessarily < 360, but >= 0), s \in [0,1], v \in [0,1]

vec3 convertRGBtoHSV(vec3 colorRGB){

	//pull coefficients
	float r = colorRGB.x;
	float g = colorRGB.y;
	float b = colorRGB.z;

	//get max and min
	float maxc = max(r,max(g,b));
	float minc = min(r,min(g,b));
	
	//v = maxc
	float v = maxc;
	
	//renormalize and recompute max and min
	r /= v; g /= v; b /= v;
	maxc = max(r,max(g,b));
	minc = min(r,min(g,b));
	
	//s = maxc - minc
	float s = maxc - minc;

	//compute hue
	float h = 0.0;
	float d = maxc - minc;
	
	if(r > g){
		if(r > b){	h =       (g-b)/d; } //r is maxc
		else{		h = 4.0 + (r-g)/d; } //b is maxc
	}else{
		if(g > b){	h = 2.0 + (b-r)/d; } //g is maxc
		else{		h = 4.0 + (r-g)/d; } //b is maxc
	}
	
	h *= 60.0;
	if(h < 0.0){ h += 360.0; }
	
	//return HSV color
	return vec3(h,s,v);
}

vec3 convertHSVtoRGB(vec3 colorHSV){

	float hh = mod(colorHSV.x, 360.0)/60.0;
	
	int i = int(hh);
	float ff = hh - float(i);
	
	//pull s, v
	float s = clamp(colorHSV.y, 0.0, 1.0);
	float v = clamp(colorHSV.z, 0.0, 1.0);
	
	float p = v * (1.0 - s);
	float q = v * (1.0 - s*ff);
	float t = v * (1.0 - s*(1.0 - ff));
	
	if(i == 0){			return vec3(v, t, p); }
	else if(i == 1){	return vec3(q, v, p); }
	else if(i == 2){	return vec3(p, v, t); }
	else if(i == 3){	return vec3(p, q, v); }
	else if(i == 4){	return vec3(t, p, v); }
	else{				return vec3(v, p, q); }
}

void main()
{
	vec4 color = texture2D(texColor, texCoord);
	vec3 colorHSV = convertRGBtoHSV(color.rgb);
	
	colorHSV = vec3(
		colorHSV.x + hsvAdjustHue,
		colorHSV.y * hsvAdjustSaturation,
		colorHSV.z * hsvAdjustValue
	);
	
	gl_FragColor = vec4(convertHSVtoRGB(colorHSV), color.a); 
}