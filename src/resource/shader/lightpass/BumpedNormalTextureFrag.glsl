
uniform sampler2D texBump;

varying vec3 tangentS;
varying vec3 tangentT;


vec3 getBumpedNormal(vec3 normalizedNormal, vec2 texCoordParallax){
	
    //get bumped normal
    vec4 bump = texture2D(texBump, texCoordParallax);
	
	return normalize(
		(bump.r - 0.5) * normalize(tangentS) + 
		(bump.g - 0.5) * normalize(tangentT) + 
		(bump.b - 0.5) * normalizedNormal);
}