#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;

varying vec3 normal;
varying float intensityFog; 


#shared shadowFilter
#shared getPhong
#shared getParallax
#shared getBumpedNormal


//have experienced strange behavior with this shader in the past:
//changing structure of ifs fixed it; probably has to do with tex coord in non-uniform flow
//see e.g. http://www.opengl.org/wiki/GLSL_Sampler#Non-uniform_flow_control

void main()
{
	vec3 normalizedNormal = normalize(normal);
	
	//perform parallax mapping
	vec2 texCoordParallax;
	vec3 fragPositionParallax;
	float maxNegativeHeightOffset;
	getParallax(normalizedNormal,
		texCoordParallax, fragPositionParallax, maxNegativeHeightOffset);
	
	//perform shadow mapping (adjust position to avoid z-fighting)
	vec3 fragPositionSM = fragPositionParallax + (maxNegativeHeightOffset + 0.05)*normalizedNormal;
	
	float fragLightPercentage;
	bool fragShadowed;
	shadowFilter(fragPositionSM, normalizedNormal, fragLightPercentage, fragShadowed);
	
	if(fragShadowed){ discard; } 
	//if(fragShadowed){ gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0); } 
	else{
	
		//read color from texture
    	vec4 color = texture2D(texColor, texCoordParallax);
    	vec3 mtlColor = color.rgb;
    	
    	if(color.a < 0.1){ discard; }
    	//if(color.a < 0.1){ gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0); }
    	else{
    	
    		//do phong lighting
			vec3 bumpedNormal = getBumpedNormal(normalizedNormal, texCoordParallax);
			vec3 phongColor = getPhong(mtlColor, bumpedNormal, fragPositionParallax);
			phongColor *= fragLightPercentage * (1.0 - intensityFog);
			
			gl_FragColor = vec4(phongColor, 1.0);
    	}
	}
}