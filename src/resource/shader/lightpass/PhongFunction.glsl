
uniform vec3 eyePosition;
uniform vec3 lightPosition;
uniform vec3 lightColor;
uniform vec3 lightAttenuation;

uniform float mtlDiffuseCoefficient;
uniform float mtlSpecularCoefficient;
uniform float mtlSpecularExponent;


vec3 getPhong(vec3 mtlColor, vec3 normal, vec3 fragPosition){
	
	//frag to light vector and distance
	vec3 fragToLightVector = lightPosition - fragPosition;	
	float dLight = length(fragToLightVector);	
	vec3 normalizedFragToLightVector = fragToLightVector / dLight;

	//compute color
    vec3 colorDiffuse = mtlColor * lightColor;
    vec3 colorSpecular = mtlColor * lightColor;

    //compute light attenuation
    float invAttenuation = dot(lightAttenuation, vec3(1.0, dLight, dLight*dLight));
    float factorAttenuation = clamp(1.0/invAttenuation, 0.0, 1.0);
    
    //compute diffuse intensity
    float intensityDiffuse = clamp(dot(normal, normalizedFragToLightVector), 0.0, 1.0);
    
    //compute specular intensity
    vec3 normalizedFragToEyeVector = normalize(eyePosition - fragPosition);
    vec3 blinnH = normalize(normalizedFragToLightVector + normalizedFragToEyeVector);
    float intensitySpecular = pow(clamp(dot(normal, blinnH), 0.0, 1.0), mtlSpecularExponent);
    
    //combine to final color
    return factorAttenuation * (
    	mtlDiffuseCoefficient * intensityDiffuse * colorDiffuse + 
    	mtlSpecularCoefficient * intensitySpecular * colorSpecular
    );
}