
uniform float fogDensity;

//strictly speaking this should be computed per fragment, since 
//it is non-linear, but it hardly makes a difference visually

float getFogIntensity(float fragCamZ){

	return 1.0 - exp(-fragCamZ*fogDensity);
}