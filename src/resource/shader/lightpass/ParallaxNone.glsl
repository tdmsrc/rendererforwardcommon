
varying vec3 vertexPosAmbient;
varying vec2 texCoord;


void getParallax(
	in vec3 normalizedNormal,
	out vec2 texCoordParallax, out vec3 fragPositionParallax, out float maxNegativeHeightOffset){

	//do nothing
	texCoordParallax = texCoord;
	fragPositionParallax = vertexPosAmbient;

	maxNegativeHeightOffset = 0.0;
}