
uniform sampler2D texBump;
uniform float reliefMappingHeight;

uniform vec3 eyePosition;

varying vec3 tangentS;
varying vec3 tangentT;

varying vec3 vertexPosAmbient;
varying vec2 texCoord;


//same principle as parallax occlusion mapping, but effectively making the assumption
//that the ray's nearest obstruction occurs at a depth equal to the original fragment's 	
	
void getParallax(
	in vec3 normalizedNormal,
	out vec2 texCoordParallax, out vec3 fragPositionParallax, out float maxNegativeHeightOffset){
	
	
	//initialize
	vec3 fragPosition = vertexPosAmbient;
	vec3 eyeToFragVector = fragPosition - eyePosition;
	
	vec3 tau = reliefMappingHeight*(normalizedNormal - eyeToFragVector/dot(eyeToFragVector,normalizedNormal));
	//assumes tangentS, tangentT are orthogonal
	vec2 tauTex = vec2(dot(tau,tangentS), dot(tau,tangentT));
	
	float texUnitsPerAmbientUnit = 0.2;
	float texelsPerTexUnit = 1024.0;
	
	
	//in and out form a subsegment of the ray from cam to frag, which is to be "traced"
	vec3 outPos,inPos;
	vec2 outTex,inTex;
	
	//if(extrude positive)
	//	outPos = fragPosition - tau + reliefMappingHeight*normalizedNormal; 
	//	outTex = texCoord - texUnitsPerAmbientUnit*tauTex;
	//	inPos = fragPosition;
	//	inTex = texCoord;
	//else if(extrude negative)
		outPos = fragPosition;
		outTex = texCoord; 
		inPos = fragPosition + tau - reliefMappingHeight*normalizedNormal;
		inTex = texCoord + texUnitsPerAmbientUnit*tauTex;
	//end if
	
	
	//get lerp (lerp 0 is "out", lerp 1 is "in") as depth of current frag
	float h = texture2D(texBump, texCoord).a;
	float lerp = 1.0-h;
	
	
	//return
	texCoordParallax = mix(outTex, inTex, lerp);
	fragPositionParallax = mix(outPos, inPos, lerp);
	
	//if(extrude positive)
	//	maxNegativeHeightOffset = 0.0;
	//else if(extrude negative)
		maxNegativeHeightOffset = reliefMappingHeight;
	//end if
}