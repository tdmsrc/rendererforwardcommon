attribute vec3 vertexPosition;
attribute vec2 vertexTex;
attribute vec3 vertexNormal;

uniform vec3 eyePosition;
uniform mat3 modelViewRotation;
uniform mat4 projectionMatrix;
uniform mat3 objRotation;
uniform vec3 objTranslation;
uniform vec3 objOffset;

varying vec3 vertexPosAmbient;
varying vec2 texCoord;
varying vec3 normal;
varying float intensityFog; 


#shared getObjRotation
#shared setTangentVectors
#shared getFogIntensity


void main(){

	texCoord = vertexTex;
	mat3 objRotationAdj = getObjRotation();

	normal = normalize(objRotationAdj * vertexNormal);
	setTangentVectors(objRotationAdj);

	vertexPosAmbient = objRotationAdj * (vertexPosition - objOffset) + objTranslation;
	vec3 vertexPosCam = modelViewRotation * (vertexPosAmbient - eyePosition);
	
	intensityFog = getFogIntensity(-vertexPosCam.z);
	
	gl_Position = projectionMatrix * vec4(vertexPosCam, 1.0);
}