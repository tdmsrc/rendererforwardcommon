
uniform sampler2D texShadowMap;

uniform vec3 lightPosition;
uniform mat4 lightProjectionMatrix;
uniform mat3 lightModelViewRotation;


bool isFragShadowed(vec3 fragPosition){
	
	//get frag position in light's clip coordinates
	vec4 fragPositionHomogLCC = lightProjectionMatrix * vec4(lightModelViewRotation * (fragPosition - lightPosition), 1.0);
	vec3 fragPositionLCC = vec3(fragPositionHomogLCC / fragPositionHomogLCC.w);
	
	//check if this outside the light's frustum
	if(	fragPositionLCC.z >  1.0 ||
		fragPositionLCC.x < -1.0 || fragPositionLCC.x > 1.0 ||
		fragPositionLCC.y < -1.0 || fragPositionLCC.y > 1.0 ){
		 
		return true;
	}
	
	//rescale from 0 to 1
	vec3 fragPositionLCCAdjusted = (fragPositionLCC + vec3(1.0, 1.0, 1.0))/2.0;
	
	
	//read in shadow map and compare
	float lightMapZBuffer = texture2D(texShadowMap, fragPositionLCCAdjusted.xy).z;
	return (lightMapZBuffer < fragPositionLCCAdjusted.z);
}