
uniform sampler2D texBump;
uniform float reliefMappingHeight;

uniform vec3 eyePosition;

varying vec3 tangentS;
varying vec3 tangentT;

varying vec3 vertexPosAmbient;
varying vec2 texCoord;


//this method is fast but assumes that if a point along the ray is unobstructed,
//so are all points before it--i.e., the ray stays obstructed after the first obstruction.
	
void getParallax(	
	in vec3 normalizedNormal,
	out vec2 texCoordParallax, out vec3 fragPositionParallax, out float maxNegativeHeightOffset){
	
	//initialize
	vec3 fragPosition = vertexPosAmbient;
	vec3 eyeToFragVector = fragPosition - eyePosition;

	vec3 tau = reliefMappingHeight*(normalizedNormal - eyeToFragVector/dot(eyeToFragVector,normalizedNormal));
	//assumes tangentS, tangentT are orthogonal
	vec2 tauTex = vec2(dot(tau,tangentS), dot(tau,tangentT));
	
	float texUnitsPerAmbientUnit = 0.2;
	float texelsPerTexUnit = 1024.0;
	
	
	//in and out form a subsegment of the ray from cam to frag, which is to be "traced"
	vec3 outPos,inPos;
	vec2 outTex,inTex;
	
	//if(extrude positive)
	//	outPos = fragPosition - tau + reliefMappingHeight*normalizedNormal;
	//	outTex = texCoord - texUnitsPerAmbientUnit*tauTex;  
	//	inPos = fragPosition;
	//	inTex = texCoord;
	//else if(extrude negative)
		outPos = fragPosition;
		outTex = texCoord; 
		inPos = fragPosition + tau - reliefMappingHeight*normalizedNormal;
		inTex = texCoord + texUnitsPerAmbientUnit*tauTex;
	//end if
	
	//get lerp (lerp 0 is "out", lerp 1 is "in") by splitting segment in half repeatedly
	float lerpOut = 0.0, lerpIn = 1.0;
	float rayTexelLength = texUnitsPerAmbientUnit * texelsPerTexUnit * length(tau);
	
	//int maxSteps = 20;
	for(int i=0; i<20; i++){
		float lerpSample = (lerpOut + lerpIn) * 0.5;
		
		float sampleHeight = texture2D(texBump, mix(outTex, inTex, lerpSample)).a;
		float sampleRayHeight = 1.0 - lerpSample;
		
		if(sampleHeight >= sampleRayHeight){ lerpIn = lerpSample; }
		else{ lerpOut = lerpSample; } //this is where the assumption in the comments above is made
		
		rayTexelLength *= 0.5;
		if(rayTexelLength <= 1.0){ break; }
	}
	
	float lerp = lerpIn;
	
	
	//return
	texCoordParallax = mix(outTex, inTex, lerp);
	fragPositionParallax = mix(outPos, inPos, lerp);
	
	//if(extrude positive)
	//	maxNegativeHeightOffset = 0.0;
	//else if(extrude negative)
		maxNegativeHeightOffset = reliefMappingHeight;
	//end if
}