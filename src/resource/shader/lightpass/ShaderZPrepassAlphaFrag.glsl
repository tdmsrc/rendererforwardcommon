#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;
uniform vec3 fogColor;
uniform float opacity;

varying float intensityFog; 
varying vec2 texCoord;


void main()
{
	float alpha = texture2D(texColor, texCoord).a;
    if(alpha < 0.1){ discard; } //ignore if masked
	
	gl_FragColor = vec4(fogColor*intensityFog, opacity*alpha);
}