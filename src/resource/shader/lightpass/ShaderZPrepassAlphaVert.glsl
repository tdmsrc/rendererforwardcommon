attribute vec3 vertexPosition;
attribute vec2 vertexTex;

uniform vec3 eyePosition;
uniform mat3 modelViewRotation;
uniform mat4 projectionMatrix;
uniform mat3 objRotation;
uniform vec3 objTranslation;
uniform vec3 objOffset;

varying vec2 texCoord;
varying float intensityFog;


#shared getObjRotation
#shared getFogIntensity


void main() {

	texCoord = vertexTex;
	mat3 objRotationAdj = getObjRotation();

	vec3 vertexPosAmbient = objRotationAdj * (vertexPosition - objOffset) + objTranslation;
	vec3 vertexPosCam = modelViewRotation * (vertexPosAmbient - eyePosition);
	 
	intensityFog = getFogIntensity(-vertexPosCam.z);
	
	gl_Position = projectionMatrix * vec4(vertexPosCam, 1.0);
}