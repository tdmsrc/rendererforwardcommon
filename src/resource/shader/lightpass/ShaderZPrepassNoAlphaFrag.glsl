#ifdef GL_ES
precision highp float;
#endif

uniform vec3 fogColor;

varying float intensityFog; 


void main()
{
	gl_FragColor = vec4(fogColor*intensityFog, 1.0);
}