
attribute vec3 vertexTangentS;
attribute vec3 vertexTangentT;

varying vec3 tangentS;
varying vec3 tangentT;


void setTangentVectors(mat3 objRotationAdj){

	tangentS = normalize(objRotationAdj * vertexTangentS);
	tangentT = normalize(objRotationAdj * vertexTangentT);
}