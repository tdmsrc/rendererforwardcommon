
uniform vec3 lightPosition;


#shared isFragShadowed


void shadowFilter(
	in vec3 fragPosition, in vec3 normalizedNormal, 
	out float fragLightPercentage, out bool fragShadowed){

	fragLightPercentage = 1.0;
	fragShadowed = isFragShadowed(fragPosition);
}
