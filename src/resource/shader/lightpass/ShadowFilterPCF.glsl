
uniform vec3 lightPosition;


#shared isFragShadowed


//see Vector3d.getOrthogonalVector
vec3 getPerp(vec3 normalizedVector){

	if((normalizedVector.x > -0.6) && (normalizedVector.x < 0.6)){
		return vec3(0, normalizedVector.z, -normalizedVector.y);
	}else if((normalizedVector.y > -0.6) && (normalizedVector.y < 0.6)){
		return vec3(-normalizedVector.z, 0, normalizedVector.x);
	}else{
		return vec3(normalizedVector.y, -normalizedVector.x, 0);
	}
}

void shadowFilter(
	in vec3 fragPosition, in vec3 normalizedNormal, 
	out float fragLightPercentage, out bool fragShadowed){

	vec3 perp = getPerp(normalizedNormal);
	vec3 perp2 = cross(normalizedNormal, perp);
	
	vec3 fragToLightVector = lightPosition - fragPosition;
	float sampleRadius = .05; //length(fragToLightVector)*.01;
	
	vec3 pcfKernel[5];
	pcfKernel[0] = -1.0*sampleRadius*perp +  0.0*sampleRadius*perp2;
	pcfKernel[1] =  1.0*sampleRadius*perp +  0.0*sampleRadius*perp2;
	pcfKernel[2] =  0.0*sampleRadius*perp +  1.0*sampleRadius*perp2;
	pcfKernel[3] =  0.0*sampleRadius*perp + -1.0*sampleRadius*perp2;
	pcfKernel[4] =  0.0*sampleRadius*perp +  0.0*sampleRadius*perp2;
	
	fragShadowed = true;
	fragLightPercentage = 0.0;
	for(int i=0; i<5; i++){
		if(!isFragShadowed(fragPosition + pcfKernel[i])){
			fragShadowed = false;
			fragLightPercentage += 1.0;
		}
	}
	fragLightPercentage /= 5.0;
}
