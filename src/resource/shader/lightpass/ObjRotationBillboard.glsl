
uniform bool useBillboard;
uniform mat3 modelViewRotationInverse;
uniform mat3 objRotation;

mat3 getObjRotation(){

	return (useBillboard ? (objRotation * modelViewRotationInverse) : objRotation);
}