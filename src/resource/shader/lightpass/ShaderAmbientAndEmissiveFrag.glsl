#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texColor;

varying vec3 normal;
varying float intensityFog; 

uniform vec3 ambientColor;
uniform float ambientCoefficient;

uniform vec3 emissiveColor;
uniform float emissiveCoefficient;


#shared getParallax


void main()
{
	vec3 normalizedNormal = normalize(normal);
	
	//perform parallax mapping
	vec2 texCoordParallax;
	vec3 fragPositionParallax;
	float maxNegativeHeightOffset;
	getParallax(normalizedNormal,
		texCoordParallax, fragPositionParallax, maxNegativeHeightOffset);

	//get color
    vec4 color = texture2D(texColor, texCoordParallax);
    if(color.a < 0.1){ discard; } //ignore if masked
	vec3 mtlColor = color.rgb;
	
	//return color lit by ambient and emissive lighting
	vec3 colorAmbient = mtlColor * ambientColor;
	vec3 colorEmissive = mtlColor * emissiveColor;

	gl_FragColor = vec4(
		ambientCoefficient * colorAmbient + 
		emissiveCoefficient * colorEmissive, 1.0);
}