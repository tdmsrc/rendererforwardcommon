
uniform vec3 lightPosition;

uniform samplerCube texShadowMapCube;
uniform float cubeProjZNear;
uniform float cubeProjZFar;


bool isFragShadowed(vec3 fragPosition){

	//get frag to light vector
	vec3 fragToLightVector = lightPosition - fragPosition;
	
	//get what would be the (positive!) z value of frag position after
	//the transformation to draw one face of the cube lightmap
	//(assumes forward/up both set to an axis direction (+/-x, +/-y, and +/-z)) 
	float fragCubeCamZ = abs(fragToLightVector.x);
	fragCubeCamZ = max(fragCubeCamZ, abs(fragToLightVector.y));
	fragCubeCamZ = max(fragCubeCamZ, abs(fragToLightVector.z));
	 
	//get what would be the clip z value of frag position after proj matrix
	//(this assumes faces of lightmap rendered using gluPerspective(90,1,zn,zf))
	float zn = cubeProjZNear; 
	float zf = cubeProjZFar;
	float fragPositionLCCZ = (zf+zn)/(zf-zn) - 2*zf*zn/((zf-zn)*fragCubeCamZ);
	
	//transform from [-1,1] to [0,1]
	float zBufferCompare = (fragPositionLCCZ + 1.0)/2.0;

	//read cube map zBuffer value and compare
	float lightMapZBuffer = textureCube(texShadowMapCube, -fragToLightVector).z;
	return (lightMapZBuffer < zBufferCompare);
}